# DataGrowth

Generic model of data evolution during SHS project.

## Generic Project
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_GenericProject.svg">

## Primary and Derived Datas
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_Primary-Derived.svg"> 